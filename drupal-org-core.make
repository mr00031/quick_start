# API
# --------
api: 2

# Core
# --------
core: 8.x

# Projects
# --------
projects:
  # Core		
  drupal:
    version: 8.4.4
  # Pull in install profile.
  quick_start:
    type: "profile"
    subdir: ""
    download:
      type: "git"
      url: "git@bitbucket.org:mr00031/quick_start.git"      